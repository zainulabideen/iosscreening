//
//  TodoTableViewCell.swift
//  IosScreeningTodoList
//
//  Created by admin on 24/06/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var tododescription: UILabel!
    
    @IBOutlet weak var checkBox: CheckBox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(viewModel:TodoItemToShow) -> (Void) {
        title.text = viewModel.title
        tododescription.text = viewModel.description
    }
}
