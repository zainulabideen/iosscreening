//
//  ViewController.swift
//  IosScreeningTodoList
//
//  Created by admin on 23/06/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit

class ViewController: UIViewController,TodoItemViewDelegate {
    func onTodoItemAdded(title:String,desc:String) {
        viewModel?.items.append(TodoItemViewModel(title: title, description: desc))
        tableView.reloadData()
    }
    
    lazy var searchController : UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = UIBarStyle.default
        controller.searchBar.barTintColor = UIColor.white
        controller.searchBar.backgroundColor = UIColor.white
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchResultsUpdater = self
        return controller
    })()
    var filtereditems: [TodoItemViewModel] = []
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

    @IBOutlet weak var tableView: UITableView!
    let tablecellid = "todocell"
    var viewModel:TodoViewModel?
    var searchBar:UISearchBar?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "TODO LIST"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addTodoItem))
        let nib = UINib(nibName: "TodoTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: tablecellid)
        viewModel = TodoViewModel()
        searchBar = searchController.searchBar
        tableView.tableHeaderView = searchBar
        
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filtereditems = viewModel?.items.filter { (item:TodoItemToShow) -> Bool in
            return (item.title?.lowercased().contains(searchText.lowercased()) ?? false)
            } as! [TodoItemViewModel]
      
      tableView.reloadData()
    }
    
    @objc func addTodoItem() {
        performSegue(withIdentifier: "toNewTodo", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toNewTodo" {
            let secondVC: NewTodoViewController = segue.destination as! NewTodoViewController
            secondVC.addNewTodoDelegate = self
        }
    }

}
extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
          return filtereditems.count
        }
        return viewModel?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tablecellid) as? TodoTableViewCell
        if isFiltering {
            let tododata = filtereditems[indexPath.item]
            cell?.configure(viewModel: tododata)
          cell?.checkBox.tag = indexPath.item
          cell?.checkBox.addTarget(self, action: #selector(performRowShiting), for:.touchUpInside)
            return cell!
        } else {
        let tododata = viewModel?.items[indexPath.item]
        cell?.configure(viewModel: tododata!)
        cell?.checkBox.tag = indexPath.item
        cell?.checkBox.addTarget(self, action: #selector(performRowShiting), for:.touchUpInside)
        return cell!
        }
    }
    
    @objc func performRowShiting(sender:CheckBox){
        print(sender.tag)
        let i = sender.tag
        
        let shiftIndex = viewModel?.items.count ?? 0
        if sender.isChecked {
        tableView.moveRow(at: IndexPath(item: sender.tag, section: 0), to: IndexPath(item: shiftIndex-1, section: 0))
        tableView.cellForRow(at: IndexPath(item: shiftIndex-1, section: 0))?.backgroundColor = UIColor.gray
            sender.tag = shiftIndex-1
            print(sender.tag)
            for j in i..<sender.tag{
                let cell = tableView.cellForRow(at: IndexPath(row: j, section: 0)) as? TodoTableViewCell
                let tmptag = cell?.checkBox.tag ?? 0
                cell?.checkBox.tag =  Int(tmptag) - 1
                
            }
        } else {
            tableView.cellForRow(at: IndexPath(item: sender.tag, section: 0))?.backgroundColor = UIColor.white
        }
        
        
    }
    
    
}
extension ViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    // TODO
    filterContentForSearchText(searchBar?.text ?? "")
  }
}
