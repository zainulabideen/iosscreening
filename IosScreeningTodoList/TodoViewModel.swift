//
//  TodoViewModel.swift
//  IosScreeningTodoList
//
//  Created by admin on 24/06/20.
//  Copyright © 2020 Zain. All rights reserved.
//

protocol TodoItemToShow {
    var title : String?{
        get
    }
    var description : String?{
        get
    }
    
    
}
struct TodoItemViewModel:TodoItemToShow {
    var title: String?
    
    var description: String?
    
    
}
protocol TodoItemViewDelegate {
    func onTodoItemAdded(title:String,desc:String) -> ()
}
struct TodoViewModel {
    init() {
        let item1 = TodoItemViewModel(title: "Get up early", description: "take bath and get fresh up")
        let item2 = TodoItemViewModel(title: "break fast", description: "having my breakfast")
        let item3 = TodoItemViewModel(title: "Get up early", description: "take bath and get fresh up")
        items.append(contentsOf:[item1,item2,item3])
    }
    var newTodoItem:String?
    
    var items :[TodoItemToShow]=[]
    
}
